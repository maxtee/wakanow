<?php
/**
 * Wakanow Application
 *
 * @package 
 */

require_once "../bootstrap.php";
require_once "../src/Frog.php";
require '../libs/Smarty.class.php';


$smarty = new Smarty;

//$smarty->force_compile = true;
$smarty->debugging = true;
$smarty->caching = false;
$smarty->cache_lifetime = 120;

//retrive the input
try {
	if(isset($_POST)) {
		$post = $_POST;
		$name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
		$color = filter_input(INPUT_POST, "color", FILTER_SANITIZE_STRING);
		$weight = filter_input(INPUT_POST, "weight", FILTER_SANITIZE_STRING);
		$sex = filter_input(INPUT_POST, "sex", FILTER_SANITIZE_STRING);
	}
	if(!$name || !$color || !$sex || !$weight) {
		throw new Exception('All fields are required');
	}
	//store into database
	$frog = new Frog();
	$frog->setName($name);
	$frog->setColor($color);
	$frog->setWeight($weight);
	$frog->setDate(time());
	$frog->setSex($sex);

	$entityManager->persist($frog);
	$entityManager->flush();


}catch(Exception $ex) {
	die($ex->getMessage());
}

//redirect to the index page
header('location: index.php');


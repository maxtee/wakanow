var wakanowManager = {
	delete: function(o) {
		var c = confirm('Are you sure you want to delete this frog?');
		var obj = $(o);
		var id = obj.attr('data-id');
		
		if(c ==  true){
			//use ajax to delete this
			$.ajax({
				data: {'id' : id},
				dataType: 'json',
				type: 'POST',
				url: 'delete.php',
				success: function(resp) {
					$('.error').html('Frog Deleted Successfully'+resp.msg).slideDown(function(){
						$(this).show();
					});
					setTimeout(function() {
						location.reload();
					},3000);	
				},
				error: function(err) {
					$('.success').html('Frog Deleted Successfully'+err.statusText).slideDown(function(){
						$(this).show();
					});
					setTimeout(function() {
						location.reload();
					},3000);
				}
			});
			return false;
		}
		return false;
	}
}
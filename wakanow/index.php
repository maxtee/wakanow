<?php
/**
 * Wakanow Application
 * @author FAMUREWA TAIWO 
 *
 * @package 
 */
require_once "../bootstrap.php";
require_once "../src/Frog.php";
require '../libs/Smarty.class.php';

$smarty = new Smarty;

//$smarty->force_compile = true;
$smarty->debugging = true;
$smarty->caching = false;
$smarty->cache_lifetime = 120;


//get the frogs


$frogRepository = $entityManager->getRepository('Frog');
$frogs = $frogRepository->findAll();
$total = count($frogs);

$smarty->assign('frogs', $frogs);
$smarty->assign("total", $total, true);

$smarty->display('index.tpl');

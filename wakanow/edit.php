<?php
/**
 * Wakanow Application
 *
 * @package 
 */
require_once "../bootstrap.php";
require_once "../src/Frog.php";
require '../libs/Smarty.class.php';

$smarty = new Smarty;

//$smarty->force_compile = true;
$smarty->debugging = true;
$smarty->caching = false;
$smarty->cache_lifetime = 120;

// $frogRepository = $entityManager->getRepository('Frog');
// $frogs = $frogRepository->findAll();

//get the id
$id = $_GET['id'];
if(!$id) {
	die('Could not find the id');
}
$smarty->assign('sex_values', array('', 'male', 'female'));
$smarty->assign('sex_output', array('Select', 'Male', 'Female'));
$frog = $entityManager->find('Frog', $id);
if ($frog === null) {
    echo "Frog $id does not exist.\n";
    exit(1);
}

$smarty->assign('frog', $frog, true);
$smarty->display('edit.tpl');

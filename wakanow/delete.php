<?php
/**
 * Wakanow Application
 *
 * @package 
 */
require_once "../bootstrap.php";
require_once "../src/Frog.php";
require '../libs/Smarty.class.php';

$smarty = new Smarty;

//$smarty->force_compile = true;
$smarty->debugging = true;
$smarty->caching = false;
$smarty->cache_lifetime = 120;


$response = array(
	'msg' => '',
	'code' => 400);

//get the id
$id = $_POST['id'];
if(!$id) {
	$response = array(
	'msg' => 'Could not find id',
	'code' => 400);
	return json_encode($response);
}

$frog = $entityManager->find('Frog', $id);
if ($frog === null) {
    $response = array(
	'msg' => 'Frog does not exist',
	'code' => 400);
	return json_encode($response);
}
$entityManager->remove($frog);
$entityManager->flush();

$response = array(
	'msg' => 'Frog was deleted successfully',
	'code' => 200);
$smarty->assign('resp', $response);

//redirect to the index page
// header('location: index.php');

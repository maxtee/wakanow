<?php
/* Smarty version 3.1.30, created on 2017-05-31 12:49:18
  from "C:\xampp\htdocs\smarty\wakanow\templates\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_592e9fae766234_88168684',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e09150f7cddb75726a36190110c19c25129ec83' => 
    array (
      0 => 'C:\\xampp\\htdocs\\smarty\\wakanow\\templates\\index.tpl',
      1 => 1496227729,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_592e9fae766234_88168684 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, "test.conf", "setup", 0);
?>

<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'foo'), 0, false);
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="form_wrap">
                <h2>Frog Ponds Application</h2><hr/>
                <div class="alert alert-success success" style="display: none"></div>
                <div class="alert alert-danger error" style="display: none"></div>
                <div class="clearfix">
                    <strong>Total:</strong> <span class="badge badge-success"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</span> Frogs
                    <span class="pull-right"><a href="create.php" class="btn btn-success">Add A Frog</a></span>
                </div>
                <br />
                <table class="table table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Color</th>
                            <th>Weight</th>
                            <th>Gender</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
$__section_frog_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_frog']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog'] : false;
$__section_frog_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['frogs']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_frog_0_total = $__section_frog_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_frog'] = new Smarty_Variable(array());
if ($__section_frog_0_total != 0) {
for ($__section_frog_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] = 0; $__section_frog_0_iteration <= $__section_frog_0_total; $__section_frog_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']++){
$_smarty_tpl->tpl_vars['__smarty_section_frog']->value['rownum'] = $__section_frog_0_iteration;
?>
                            <tr>
                                <td><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['rownum']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['rownum'] : null);?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getName();?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getColor();?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getWeight();?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getSex();?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getDate();?>
</td>
                                <th>
                                    <a href="edit.php?id=<?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getID();?>
" class="btn btn-primary">Edit</a>
                                    <a href="#" class="btn btn-danger"
                                    data-id="<?php echo $_smarty_tpl->tpl_vars['frogs']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_frog']->value['index'] : null)]->getID();?>
"
                                    onclick="return wakanowManager.delete(this);">Delete</a>
                                </th>
                            </tr>
                          
                        <?php
}
}
if ($__section_frog_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_frog'] = $__section_frog_0_saved;
}
?>
                    </tbody>
                </table>
                <center><h2>Designed by Famurewa Taiw0</h2><br/><h5>Email: famurewa_taiwo@yahoo.com</h5></center>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
 src="../wakanow/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="../wakanow/js/wakanow.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}

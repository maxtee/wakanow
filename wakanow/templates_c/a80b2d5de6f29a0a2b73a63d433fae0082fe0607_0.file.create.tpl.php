<?php
/* Smarty version 3.1.30, created on 2017-05-31 12:49:13
  from "C:\xampp\htdocs\smarty\wakanow\templates\create.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_592e9fa90b6a08_08059340',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a80b2d5de6f29a0a2b73a63d433fae0082fe0607' => 
    array (
      0 => 'C:\\xampp\\htdocs\\smarty\\wakanow\\templates\\create.tpl',
      1 => 1496227751,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_592e9fa90b6a08_08059340 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once 'C:\\xampp\\htdocs\\smarty\\libs\\plugins\\function.html_options.php';
$_smarty_tpl->smarty->ext->configLoad->_loadConfigFile($_smarty_tpl, "test.conf", "setup", 0);
?>

<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'foo'), 0, false);
?>


<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="form_wrap">
                <h2>Frog Ponds Application</h2><hr/>
                <div class="clearfix">
                    <span class="pull-right"><a href="index.php" class="btn btn-primary">Display Frogs</a></span>
                </div>
                <br />
                <form action="submit.php" method="post" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Frog Name:</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Color: </label>
                        <div class="col-sm-10">
                            <input type="text" name="color" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Weight: </label>
                        <div class="col-sm-10">
                            <input type="text" name="weight" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sex: </label>
                        <div class="col-sm-10">
                            <select name="sex" required="" class="form-control">
                                <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['sex_values']->value,'output'=>$_smarty_tpl->tpl_vars['sex_output']->value),$_smarty_tpl);?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-success form-control">Add Frog</button>
                        </div>
                    </div>
                </form>
                <center><h2>Designed by Famurewa Taiw0</h2><br/><h5>Email: famurewa_taiwo@yahoo.com</h5></center>
                
            </div>
        </div>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
}

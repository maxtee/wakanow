{config_load file="test.conf" section="setup"}
{include file="header.tpl" title=foo}
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="form_wrap">
                <h2>Frog Ponds Application</h2><hr/>
                <div class="alert alert-success success" style="display: none"></div>
                <div class="alert alert-danger error" style="display: none"></div>
                <div class="clearfix">
                    <strong>Total:</strong> <span class="badge badge-success">{$total}</span> Frogs
                    <span class="pull-right"><a href="create.php" class="btn btn-success">Add A Frog</a></span>
                </div>
                <br />
                <table class="table table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Color</th>
                            <th>Weight</th>
                            <th>Gender</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         {section name=frog loop=$frogs}
                            <tr>
                                <td>{$smarty.section.frog.rownum}</td>
                                <td>{$frogs[frog]->getName()}</td>
                                <td>{$frogs[frog]->getColor()}</td>
                                <td>{$frogs[frog]->getWeight()}</td>
                                <td>{$frogs[frog]->getSex()}</td>
                                <td>{$frogs[frog]->getDate()}</td>
                                <th>
                                    <a href="edit.php?id={$frogs[frog]->getID()}" class="btn btn-primary">Edit</a>
                                    <a href="#" class="btn btn-danger"
                                    data-id="{$frogs[frog]->getID()}"
                                    onclick="return wakanowManager.delete(this);">Delete</a>
                                </th>
                            </tr>
                          
                        {/section}
                    </tbody>
                </table>
                <center><h2>Designed by Famurewa Taiwo</h2><br/><h5>Email: famurewa_taiwo@yahoo.com</h5></center>
            </div>
        </div>
    </div>
</div>
<script src="../wakanow/js/jquery.js"></script>
<script src="../wakanow/js/wakanow.js"></script>
{include file="footer.tpl"}
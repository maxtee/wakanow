{config_load file="test.conf" section="setup"}
{include file="header.tpl" title=foo}

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="form_wrap">
                <h2>Frog Ponds Application</h2><hr/>
                <div class="clearfix">
                    <span class="pull-right"><a href="index.php" class="btn btn-primary">Display Frogs</a></span>
                </div>
                <br />
                <form action="update.php" method="post" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Frog Name:</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" required="" value="{$frog->getName()}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Color: </label>
                        <div class="col-sm-10">
                            <input type="text" name="color" class="form-control" required="" value="{$frog->getColor()}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Weight: </label>
                        <div class="col-sm-10">
                            <input type="text" name="weight" class="form-control" required="" value="{$frog->getWeight()}">
                            <input type="hidden" name="id" value="{$frog->getId()}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sex: </label>
                        <div class="col-sm-10">
                            <select name="sex" required="" class="form-control">
                                {html_options values=$sex_values selected=$frog->getSex() output=$sex_output }
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-success form-control">Update Frog</button>
                        </div>
                    </div>
                </form>
                <center><h2>Designed by Famurewa Taiwo</h2><br/><h5>Email: famurewa_taiwo@yahoo.com</h5></center>

            </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}


<?php
// src/Frog.php
/**
 * @Entity @Table(name="frogs")
 **/
class Frog
{
    /**
     * @var int
     */
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /**
     * @var string
     */
    /** @Column(type="string") **/
    protected $name;
    /** @Column(type="string") **/
    protected $color;
    /** @Column(type="string") **/
    protected $weight;
    /** @Column(type="integer") **/
    protected $date;
    /** @Column(type="string") **/
    protected $sex;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getColor()
    {
        return $this->color;
    }
    public function getWeight()
    {
        return $this->weight;
    }
    public function getDate()
    {
        return date('M d, Y', $this->date);
    }
    public function getSex()
    {
        return $this->sex;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function setColor($color)
    {
        $this->color = $color;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }

}